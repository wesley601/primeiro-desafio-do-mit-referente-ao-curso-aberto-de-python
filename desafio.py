"""
    para resolver este desafio, tive a ideia de transformar a string, em um vetor de numeros,
onde cada letra da string seria transformada em um número equivalente a sua posição no alfabeto.
Assim, ficou mais simples para fazer as comparações.


"""
#s = 'azcbobobegghakl'
s = input("write the string: ")   
alfabeto = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z']
sEmNumeros = []

for x in s:
    i = 0
    while i < len(alfabeto):
        if x == alfabeto[i]:
            sEmNumeros.append(i)
            break
        i+=1

"""
Tando no laço de 'f' quanto em seu subsequente, usei uma logica um pouco semelhante as de 
algotimos de ordenação, onde inicio o laço apartir no 1 e para assim poder trabalhar com 
dois valores do vetor, sem ter o erro de 'out of the range'

"""
"""
Agora para encontrar a maior substring (Como manda o enunciado) criei o vetor criticalPonts 
com o valor inicia 0 Onde armazeno os valores que "quebram" uma sequencia crenscente. Ex: 
em uma string 'abcafea'ela será convertida para '0120541' e laço salvará dentro de 
criticalPonts os indice 3.

"""
f = 1
criticalPonts = [0]

while f < len(sEmNumeros):
    i = f - 1
    if sEmNumeros[i] > sEmNumeros[f]:
        criticalPonts.append(f)
        #print(i)
    f+=1

k = 1
valorMax = 0
print(criticalPonts)
"""

E finalmente o ultimo passo que primeiro verificará se só existe um unico valor em criticalPonts,
caso não ele passará para o ultimo laco que calculará uma média entre os pontos e descobrirá qual é 
o maior espaço entre eles, ou seja, a maior substring ordenada

"""
if len(criticalPonts) == 1:
    p1 = -1
    p2 = criticalPonts[0]
else:
    while k < len(criticalPonts):
        ant = k - 1
        media =criticalPonts[k] - criticalPonts[ant]

        if media >= valorMax:
            valorMax = media
            p1 = criticalPonts[ant]
            p2 = criticalPonts[k]
        k+=1

print(f"finalmente: {s[p1:p2]}")
